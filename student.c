/******************************************************************************
 * Laborator 01 - Zaklady pocitacove grafiky - IZG
 * ihulik@fit.vutbr.cz
 *
 * $Id: $
 * 
 * Popis: Hlavicky funkci pro funkce studentu
 *
 * Opravy a modifikace:
 * - ibobak@fit.vutbr.cz, orderedDithering
 */

#include "student.h"
#include "globals.h"

#include <time.h>

const int M[] = {
    0, 204, 51, 255,
    68, 136, 187, 119,
    34, 238, 17, 221,
    170, 102, 153, 85
};

const int M_SIDE = 4;

/******************************************************************************
 ******************************************************************************
 Funkce vraci pixel z pozice x, y. Je nutne hlidat frame_bufferu, pokud 
 je dana souradnice mimo hranice, funkce vraci barvu (0, 0, 0).
 Ukol za 0.25 bodu */
S_RGBA getPixel(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height)
	{
    		return COLOR_BLACK; //vraci barvu (0, 0, 0)
	}
	else
	{
		return frame_buffer [y*width + x];
	}
}
/******************************************************************************
 ******************************************************************************
 Funkce vlozi pixel na pozici x, y. Je nutne hlidat frame_bufferu, pokud 
 je dana souradnice mimo hranice, funkce neprovadi zadnou zmenu.
 Ukol za 0.25 bodu */
void putPixel(int x, int y, S_RGBA color)
{
	if (x  >= 0 && x < width && y >= 0 && y < height)
	{
		frame_buffer [y*width + x] = color;
	}
}
/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na odstiny sedi. Vyuziva funkce GetPixel a PutPixel.
 Ukol za 0.5 bodu */
void grayScale()
{
	S_RGBA p;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			p = getPixel(x,y);
			p.red = ROUND(0.299*p.red + 0.587*p.green + 0.114*p.blue);
			p.green = p.red;
			p.blue = p.red;
			putPixel(x,y,p);
		}
	}
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci algoritmu maticoveho rozptyleni.
 Ukol za 1 bod */

void orderedDithering()
{
	grayScale();
    	S_RGBA p;
	int intensity;
	int i,j;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			p = getPixel(x,y);
			intensity = ROUND(0.299*p.red + 0.587*p.green + 0.114*p.blue);
			i = x % M_SIDE;
			j = y % M_SIDE;
			if (intensity > M [j*M_SIDE + i])
			{
				putPixel(x,y,COLOR_WHITE);
			}
			else
			{
				putPixel(x,y,COLOR_BLACK);
			}
		}
	}
}

void error(int x, int y, float E)
{
	S_RGBA p;
   	int test;
    	if (x  >= 0 && x < width && y >= 0 && y < height)
    	{
        	p = getPixel(x,y);
         	test = ROUND(p.red  + E);
        	if(test > 255) 
		{
            		putPixel(x,y, COLOR_WHITE);
            		return;
        	}
        	if(test < 0) 
		{
            		putPixel(x,y, COLOR_BLACK);
            		return;
    		}
    		else 
		{      
        		p.red = test;
        		p.green = test;
        		p.blue = test;
        		putPixel(x,y, p);
        	}
    }
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci algoritmu distribuce chyby.
 Ukol za 1 bod */

void errorDistribution()
{
	grayScale();
	S_RGBA p;
	int intensity;
	int E;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			p = getPixel(x,y);
			intensity = ROUND(0.299*p.red + 0.587*p.green + 0.114*p.blue);
			if (intensity < 127)
			{
				putPixel(x,y,COLOR_BLACK);
				E = intensity;
			}
			else
			{
				putPixel(x,y,COLOR_WHITE);
				E = intensity - 255;
			}
			
			error(x+1,y, 3.0/8.0 * E);
			error(x, y+1, 3.0/8.0 * E);
			error(x + 1, y + 1, 2.0/8.0 * E );
		}
	} 
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci metody prahovani.
 Demonstracni funkce */
void thresholding(int Threshold)
{
	/* Prevedeme obrazek na grayscale */
	grayScale();

	/* Projdeme vsechny pixely obrazku */
	for (int y = 0; y < height; ++y)
		for (int x = 0; x < width; ++x)
		{
			/* Nacteme soucasnou barvu */
			S_RGBA color = getPixel(x, y);

			/* Porovname hodnotu cervene barevne slozky s prahem.
			   Muzeme vyuzit jakoukoli slozku (R, G, B), protoze
			   obrazek je sedotonovy, takze R=G=B */
			if (color.red > Threshold)
				putPixel(x, y, COLOR_WHITE);
			else
				putPixel(x, y, COLOR_BLACK);
		}
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci nahodneho rozptyleni. 
 Vyuziva funkce GetPixel, PutPixel a GrayScale.
 Demonstracni funkce. */
void randomDithering()
{
	/* Prevedeme obrazek na grayscale */
	grayScale();

	/* Inicializace generatoru pseudonahodnych cisel */
	srand((unsigned int)time(NULL));

	/* Projdeme vsechny pixely obrazku */
	for (int y = 0; y < height; ++y)
		for (int x = 0; x < width; ++x)
		{
			/* Nacteme soucasnou barvu */
			S_RGBA color = getPixel(x, y);
			
			/* Porovname hodnotu cervene barevne slozky s nahodnym prahem.
			   Muzeme vyuzit jakoukoli slozku (R, G, B), protoze
			   obrazek je sedotonovy, takze R=G=B */
			if (color.red > rand()%255)
			{
				putPixel(x, y, COLOR_WHITE);
			}
			else
				putPixel(x, y, COLOR_BLACK);
		}
}
/*****************************************************************************/
/*****************************************************************************/
